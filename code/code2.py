#!/usr/bin/env python
# coding: utf-8

# <a href="https://colab.research.google.com/github/BehzadShayegh/ZillowPrize1/blob/master/ZillowPrize1_2.ipynb" target="_parent"><img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>

# # Zillow Prize 1

# ## Wrap Up part 3

# در این قسمت دیتایی که در قسمت قبل ساختیم را لود کرده و با آن عملیات پیشبینی را انجام می دهیم.

# ### Requirements to begin

# In[ ]:


get_ipython().run_line_magic('reload_ext', 'autoreload')
get_ipython().run_line_magic('autoreload', '2')
get_ipython().run_line_magic('matplotlib', 'inline')


# #### Load Data from Drive

# In[2]:


from google.colab import drive
drive.mount('/content/drive')


# In[3]:


get_ipython().system('pip3 install pydrive')

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from google.colab import auth
from oauth2client.client import GoogleCredentials

auth.authenticate_user()
gauth = GoogleAuth()
gauth.credentials = GoogleCredentials.get_application_default()
drive = GoogleDrive(gauth)


# In[ ]:


DATA_PATH = './drive/My Drive/'
ZIP_NAME = 'zillow-prize-1.zip'
DATASET_NAME = 'new_data_set.txt'
# DATASET_NAME = 'new_data_set (1).txt'


# #### Read new DataSet

# In[5]:


import json

data_set = dict()

with open(DATA_PATH + DATASET_NAME, "r") as f :
    data_set = json.load(f, object_hook=lambda d: {int(k) if k.lstrip('-').isdigit() else k: v for k, v in d.items()})
    
data_set


# In[6]:


len(data_set)


# In[ ]:


import numpy as np

basic_data = np.array(list(data_set.values()))


# #### Unzip and Read Train Sets

# In[8]:


import pandas as pd
from zipfile import ZipFile

zip_file = ZipFile(DATA_PATH+ZIP_NAME)

path_train_2016 = 'train_2016_v2.csv'
path_train_2017 = 'train_2017.csv'

train_2016 = pd.read_csv(zip_file.open(path_train_2016))
train_2017 = pd.read_csv(zip_file.open(path_train_2017))

train_2017.head()


# In[ ]:


parcelid_2016 = np.array(train_2016['parcelid'])
parcelid_2017 = np.array(train_2017['parcelid'])

logerror_2016 = np.array(train_2016['logerror'])
logerror_2017 = np.array(train_2017['logerror'])


# In[10]:


logerror_2016


# #### Make efficient data structures

# برای استفاده از الگوریتم های یادگیری باید ساختاری منظم از دیتای آموزش تست و نتایج داشته باشیم. پس دیتای پارسل ها را با ترتیب دیتا ها به ترتیب سال ۲۰۱۶ و ۲۰۱۷ مرتب می کنیم.

# In[ ]:


orderX2016 = np.array([data_set[pid] for pid in parcelid_2016])
orderX2017 = np.array([data_set[pid] for pid in parcelid_2017])


# In[12]:


orderX2016


# In[13]:


(orderX2016.shape,
 logerror_2016.shape,
 orderX2017.shape,
 logerror_2017.shape)


# #### Add date to features

# علاوه بر دیتای پارسل ها تاریخ معاملات نیز می تواند در خطای موجود تاثیر گذار باشد. پس آن ها را نیز به دیتای آموزش اضافه می کنیم:

# In[14]:


train_2016['transactiondate']


# In[15]:


years2016 = train_2016['transactiondate'].str.split('-').str[0].astype('int')
years2016 = np.array(years2016)
years2016 = years2016.reshape(-1,1)

mounths2016 = train_2016['transactiondate'].str.split('-').str[1].astype('int')
mounths2016 = np.array(mounths2016)
mounths2016 = mounths2016.reshape(-1,1)

print(years2016[:,0], years2016.shape)
print(mounths2016[:,0], mounths2016.shape)


# In[16]:


years2017 = train_2017['transactiondate'].str.split('-').str[0].astype('int')
years2017 = np.array(years2017)
years2017 = years2017.reshape(-1,1)

mounths2017 = train_2017['transactiondate'].str.split('-').str[1].astype('int')
mounths2017 = np.array(mounths2017)
mounths2017 = mounths2017.reshape(-1,1)

print(years2017[:,0], years2017.shape)
print(mounths2017[:,0], mounths2017.shape)


# In[17]:


orderX2016d = np.append(orderX2016, years2016, axis=1)
orderX2016d = np.append(orderX2016d, mounths2016, axis=1)
orderX2017d = np.append(orderX2017, years2017, axis=1)
orderX2017d = np.append(orderX2017d, mounths2017, axis=1)

orderX2016d.shape, orderX2017d.shape


# دیتا ها را با هم تلفیق می کنیم.

# In[18]:


orderX = np.append(orderX2016d, orderX2017d, axis=0)
orderY = np.append(logerror_2016, logerror_2017, axis=0)
orderX.shape, orderY.shape


# حال زمان طراحی هوش مصنوعی برای آموزش دیتاست. در این بخش ما عملکرد دو روش را آزمودیم :

# ### Build Predictor

# در ابتدا نیاز است قسمتی از دیتا را برای تست جدا کنیم :

# In[ ]:


testIndexes = np.random.choice(orderY.shape[0], int(orderY.shape[0]/5))
testBools = np.zeros(orderY.shape[0]).astype(bool)
testBools[testIndexes] = True


# In[20]:


testX = orderX[testBools]
testY = orderY[testBools]

trainX = orderX[np.logical_not(testBools)]
trainY = orderY[np.logical_not(testBools)]

testX.shape, testY.shape, trainX.shape, trainY.shape


# #### Random Forest

# ابتدا از روش جنگل تصادفی استفاده کردیم که نتایجی دقیقا مشابه قسمت اول داشت. دقت بالای ۸۰ برای دیتای آموزش و دقت ۲۰ برای تست که قابل قبول نیست. تفاوت تکمیل دیتا برای این متد تنها سریع تر شدن آن به دلیل حذف برخی ستون ها بود. پس این روش ناکارآ را کنار گذاشتیم.

# In[ ]:


# from sklearn.ensemble import RandomForestRegressor

# regr = RandomForestRegressor(n_estimators=1000)

# regr.fit(trainX, trainY)
# print(regr.score(trainX, trainY))
# print(regr.score(testX, testY))


# #### Neural Network

# در این بخش ما یک شبکه ی عصبی برای حل این موضوع می سازیم. تمام هایپر پارامتر های این شبکه با آزمون و خطا های فراوان بدست آمده اند ولی فرایند به دست آوردن آن ها (آزمون و خطا ها) به دلیل حجم زاید گزارش کار آورده نشده اند.

# ##### Requrements to design NN

# برای این شبکه از پردازش گر گرافیکی کودا استفاده می کنیم. پس ابتدا دیتا را به فرمتی مناسب در می آوریم. :

# In[ ]:


import torch

trainXt = torch.from_numpy(trainX)
trainYt = torch.from_numpy(trainY)
testXt = torch.from_numpy(testX)
testYt = torch.from_numpy(testY)


# In[ ]:


from torch.autograd import Variable
from torch import nn
import torch.nn.functional as F
import torch.utils.data as Data


# In[24]:


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
device


# برای کار با دیتاست آن ها را به لودر ها تبدیل می کنیم تا بتوانیم به صورتی مناسب با بچ سایز ها کار کنیم.

# In[ ]:


torch.manual_seed(1)
loss_func = torch.nn.MSELoss()

train_dataset = Data.TensorDataset(Variable(trainXt), Variable(trainYt))
test_dataset = Data.TensorDataset(Variable(testXt), Variable(testYt))

BATCH_SIZE = 1000
trainLoader = Data.DataLoader(
    dataset=train_dataset, 
    batch_size=BATCH_SIZE, 
    shuffle=True, num_workers=2,)

testLoader = Data.DataLoader(
    dataset=test_dataset, 
    batch_size=BATCH_SIZE, 
    shuffle=False, num_workers=2,)


# توابع آموزش و تست مناسبی طراحی می کنیم :

# In[ ]:


from math import floor

def Train(Net, optimizer, loader, EPOCH=1000) :
  
  for epoch in range(EPOCH) :
    for step, (batch_x, batch_y) in enumerate(loader) :

      b_x = Variable(batch_x).to(device)
      b_x = b_x.float()
      b_y = Variable(batch_y).to(device)
      b_y = b_y.float()

      prediction = Net(b_x)
      loss = loss_func((prediction).to(device), b_y)

      optimizer.zero_grad()
      loss.backward()
      optimizer.step()

    print('epoch:', epoch, 'step:', step, 'loss:', loss.item())


# In[ ]:


def Test(NET, loader) :
  error = 0
  total = 0
  with torch.no_grad():
    for step, (batch_x, batch_y) in enumerate(loader) :
      
      b_x = Variable(batch_x).to(device)
      b_x = b_x.float()
      b_y = Variable(batch_y).to(device)
      b_y = b_y.float()
      
      outputs = NET(b_x)
      _, predicted = torch.max(outputs.data, 1)
      predicted = predicted.float()
      
      total += b_y.size(0)
      error += sum(abs(predicted - (b_y)))
    
    error /= total
    
  print('Error of the network on the test inputs:', error)


# ##### Design Neural Network

# حال زمان طراحی شبکه عصبی است. شبکه ی زیر حاصل آزمون ده ها شبکه ی متفاوت است. البته شبکه ای با اندازه ی کمی کوچک تر نیز با سرعت بسیار خوبی به نتیجه ی تقریبا مشابه می رسید اما ما این شبکه را انتخاب کردیم.

# In[ ]:


inputSize = trainXt.shape[1]

class Net(nn.Module):
  def __init__(self):
    super(Net, self).__init__()
    self.fc1 = nn.Linear(inputSize, 32)
    self.fc2 = nn.Linear(32, 64)
    self.fc3 = nn.Linear(64, 64)
    self.fc4 = nn.Linear(64, 64)
    self.fc5 = nn.Linear(64, 128)
    self.fc6 = nn.Linear(128, 128)
    self.fc7 = nn.Linear(128, 128)
    self.fc8 = nn.Linear(128, 256)
    self.fc9 = nn.Linear(256, 256)
    self.fc10= nn.Linear(256, 256)
    self.fc11= nn.Linear(256, 256)
    self.fc12= nn.Linear(256, 128)
    self.fc13= nn.Linear(128, 128)
    self.fc14= nn.Linear(128, 128)
    self.fc15= nn.Linear(128, 64)
    self.fc16= nn.Linear(64, 64)
    self.fc17= nn.Linear(64, 64)
    self.fc18= nn.Linear(64, 32)
    self.fc19= nn.Linear(32, 16)
    self.fc20= nn.Linear(16, 8)
    self.fc21= nn.Linear(8, 1)
    
  def forward(self, x):
    x = F.leaky_relu(self.fc1(x))
    x = F.leaky_relu(self.fc2(x))
    x = F.leaky_relu(self.fc3(x))
    x = F.leaky_relu(self.fc4(x))
    x = F.leaky_relu(self.fc5(x))
    x = F.leaky_relu(self.fc6(x))
    x = F.leaky_relu(self.fc7(x))
    x = F.leaky_relu(self.fc8(x))
    x = F.leaky_relu(self.fc9(x))
    x = F.leaky_relu(self.fc10(x))
    x = F.leaky_relu(self.fc11(x))
    x = F.leaky_relu(self.fc12(x))
    x = F.leaky_relu(self.fc13(x))
    x = F.leaky_relu(self.fc14(x))
    x = F.leaky_relu(self.fc15(x))
    x = F.leaky_relu(self.fc16(x))
    x = F.leaky_relu(self.fc17(x))
    x = F.leaky_relu(self.fc18(x))
    x = F.leaky_relu(self.fc19(x))
    x = F.leaky_relu(self.fc20(x))
    x = self.fc21(x)
    return x

net = Net().to(device)


# ##### Train Neural Network

# حال به آموزش شبکه می پردازیم.

# In[29]:


optimizer = torch.optim.Adam(net.parameters(), lr=1e-3)
Train(net, optimizer, trainLoader, EPOCH=100)


# همانطور که مشاهده می شود. شاهد نتیجه ی قابل قبولی هستیم.
# 
# پس شبکه را تست می کنیم

# In[30]:


Test(net, testLoader)


# قابل قبول است.
# 
# قبل از اجرای فرایند پیشبینی اصلی بد نیست دیتای تست را نیز به شبکه آموزش دهیم :

# In[ ]:


testLoader2 = Data.DataLoader(
    dataset=test_dataset, 
    batch_size=BATCH_SIZE, 
    shuffle=False, num_workers=2,)


# In[32]:


optimizer = torch.optim.Adam(net.parameters(), lr=1e-4)
Train(net, optimizer, testLoader2, EPOCH=100)


# ### Predict logerror

# هدف پیشبینی تک تک پارسل ها در ماه های 10و11و12 دو سال هست که ما میان هر ماه را حساب میکنیم. پس نیاز است ستون تاریخ را که به دیتا ست اضافه کردیم هربار به یکی از تاریخ ها تبدیل کنیم و عملیات پیشبینی را انجام دهیم :

# In[ ]:


date201610 = []
date201611 = []
date201612 = []
date201710 = []
date201711 = []
date201712 = []

forPredict = [date201610,date201611,date201612, date201710,date201711,date201712]

yearsList = [2016, 2016, 2016, 2017, 2017, 2017]
mounthsList = [10, 11, 12, 10, 11, 12]


# In[34]:


basic_data.shape


# In[ ]:


trainXt = torch.from_numpy(trainX)

for i in range(6) :
  with torch.no_grad():
      years = (np.array([yearsList[i]]*(basic_data.shape[0]))).reshape(-1,1)
      mounths = (np.array([mounthsList[i]]*(basic_data.shape[0]))).reshape(-1,1)
      this_data = np.append(basic_data, years, axis=1)
      this_data = np.append(this_data, mounths, axis=1)
      this_data = torch.from_numpy(this_data)
      this_data = Variable(this_data).to(device).float()

      outputs = net(this_data)
      forPredict[i] = outputs.data.tolist()


# In[51]:


forPredict = np.array(forPredict)
forPredict = forPredict.reshape(forPredict.shape[0], forPredict.shape[1])
forPredict = forPredict.tolist()
forPredict


# نتیجه آماده است.

# #### Save result

# In[ ]:


from pandas import DataFrame

result = {'ParcelId': list(data_set.keys()),
        '201610': forPredict[0],
        '201611': forPredict[1],
        '201612': forPredict[2],
        '201710': forPredict[3],
        '201711': forPredict[4],
        '201712': forPredict[5] }

export_csv = DataFrame(result, columns= ['ParcelId','201610','201611','201612','201710','201711','201712'])            .to_csv('result.csv', index = None, header=True)

