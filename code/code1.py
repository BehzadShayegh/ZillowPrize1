#!/usr/bin/env python
# coding: utf-8

# <a href="https://colab.research.google.com/github/BehzadShayegh/ZillowPrize1/blob/master/ZillowPrize1_1.ipynb" target="_parent"><img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>

# # Zillow Prize 1

# ## Wrap Up part 2

# در این قسمت با توجه به عدم موفقیت در قسمت قبل بر آن آمده ایم تا نقصان دیتای موجود را با روشی معقول تر برطرف کنیم و دیتاست تمییز تری آماده کنیم.

# ### Requirements to begin

# In[ ]:


get_ipython().run_line_magic('reload_ext', 'autoreload')
get_ipython().run_line_magic('autoreload', '2')
get_ipython().run_line_magic('matplotlib', 'inline')


# ابتدا باید دیتاست اصلی را دانلود کنیم.

# #### Download Data with API

# این قسمت برای دانلود مستقیم از سایت کگل می باشد. اما ما دیتا را از روی درایو گوگل فراخوانی می کنیم.

# In[ ]:


# !pip install -U -q kaggle
# !mkdir -p ~/.kaggle


# In[ ]:


# from google.colab import files
# files.upload()


# In[ ]:


# !cp kaggle.json ~/.kaggle/


# In[ ]:


# !kaggle datasets list


# In[ ]:


# !kaggle competitions download -c zillow-prize-1
# !ls
# DATA_PATH = './'


# #### Load Data from Drive

# In[ ]:


from google.colab import drive
drive.mount('/content/drive')


# In[ ]:


get_ipython().system('pip3 install pydrive')

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from google.colab import auth
from oauth2client.client import GoogleCredentials

auth.authenticate_user()
gauth = GoogleAuth()
gauth.credentials = GoogleCredentials.get_application_default()
drive = GoogleDrive(gauth)


# In[ ]:


DATA_PATH = './drive/My Drive/'


# #### Unzip and read Data

# In[ ]:


ZIP_NAME = 'zillow-prize-1.zip'


# In[ ]:


import pandas as pd
from zipfile import ZipFile

zip_file = ZipFile(DATA_PATH+ZIP_NAME)

path_properties_2016 = 'properties_2016.csv'
path_properties_2017 = 'properties_2017.csv'

# properties_2016 = pd.read_csv(zip_file.open(path_properties_2016))
properties_2017 = pd.read_csv(zip_file.open(path_properties_2017))


# #### Initial analysis

# بد نیست نگاهی دقیق تر به دیتاست داشته باشیم. ابتدا با خط کد زیر اجازه ی دسترسی به دیتای هر پارسل را با توجه به آیدی آن فراهم می آوریم :

# In[ ]:


properties_2017.set_index('parcelid', inplace=True)


# In[ ]:


units_number, cols_number = properties_2017.shape
print(units_number, cols_number)


# روش انتخابی ما برای پر کردن نقصان دیتا رگرسیون قدم به قدم هر ستون دیتاست. اما بعضی از این ستون ها ناقص تر از آن اند که بتوان روی آن ها رگرسیون انجام داد. پس باید ابتدا ستون های قابل تحلیل را تشخیص دهیم. پس اول پارامتری برای تعیین میزان نقصان هر ستون بدست می آوریم :

# In[ ]:


incompletenes_edge = 40
most_complete_cols = []
incomplete_rate = {}

for col in properties_2017.columns :
  incomplete_value = properties_2017[col].isna().sum()
  incomplete_rate[col] = incomplete_value * 100 / units_number
  if incomplete_rate[col] < incompletenes_edge :
    most_complete_cols.append(col)
    
print('columns incomplete rates :\n')
incomplete_rate


# مرز مورد قبول ما برای درصد نقصان برابر ۴۰ درصد می باشد بدین معنا که تحلیل را فقط برای ستون هایی با درصد تکامل ۶۰ به بالا بررسی می کنیم. این ستون ها عبارت اند از :

# In[ ]:


print('most complete cols (with', completenes_edge,'% rate ) :\n')
most_complete_cols


# همانند قسمت قبل بازهم ترجیح ما استفاده از دیتا های عددی برای رگرسیون است. پس بار دیگر دیتا های غیر عددی را نیز علاوه بر ستون های ناقص نادیده می گیریم.

# In[ ]:


properties_2017.dtypes


# In[ ]:


most_complete_float_dtypes = (properties_2017[most_complete_cols].dtypes == 'float64')

from itertools import compress
train_select_features = list(compress(most_complete_cols, most_complete_float_dtypes.values))

print('so we will train these features for 2016:\n')
train_select_features


# پس فقط ستون های فوق در الگوریتم ما شرکت خواهند کرد. برای کامل کردن ستون ها با روش رگرسیون خطی برای رسیدن به کمترین خطا بهتر است از ستونی شروع کنیم که نقصان کمتری دار. پس ستون ها را بر این حسب مرتب می کنیم :

# In[ ]:


import operator
complete_sorted_features = sorted(incomplete_rate, key=operator.itemgetter(1))
sorted_selected_train_features = [i for i in complete_sorted_features if i in train_select_features]

print('We will complete data in following order:\n')
sorted_selected_train_features


# In[ ]:


properties_2017[sorted_selected_train_features].head()


# پس کافیست یک الگوریتم رگرسیون خطی را با ترتیب بالا بر روی ستون ها اجرا کنیم تا به دیتا ست کاملی برسیم.

# ### Guess invalid fetures

# #### Regresion

# دیتایی که موضف به کامل کردن آن هستیم را جدا می کنیم.

# In[ ]:


properties = properties_2017[sorted_selected_train_features].copy()


# سیاست ما برای انجام عمل جایگذاری با رگرسیون این است که با توجه به ناقص بودن تمام ستون ها ابتدا دیتای تمام ستون های غیر از ستونی که میخواهیم روی آن رگرسیون انجام دهیم را با متد میانگین دیتا ها پر می کنیم و حال با این دیتای تقریبی با یک متد رگرسیون (دیفالت خطی) حدسی برای دیتا های ستون مرود نظر می زنیم. با توجه به بازدهی این حدس ها بر دیتای تست (که باز هم از همان ستون از دانسته ها جدا شده اند) تصمیم می گیریم که این حدس را جایگذاری کنیم یا این کار را به زمان عقب تری موکول کنیم :

# In[ ]:


import numpy as np
from math import floor
from sklearn.linear_model import LinearRegression
from sklearn.impute import SimpleImputer


def guess_feature(i, trust_edge=0.9, method=LinearRegression, strategy='mean') :
  imputer = SimpleImputer(strategy=strategy)
  
  xLabels = sorted_selected_train_features[:i]+sorted_selected_train_features[i+1:]
  yLabel = sorted_selected_train_features[i]
  
  print('_______________')
  print('for feature :', yLabel)
  print('_______________')

  X = properties[xLabels].values
  Y = properties[yLabel].values

  toPredict = np.zeros(Y.shape,dtype=bool)
  toPredict[np.where(np.isnan(Y))[0]] = True

  predictNumber = np.sum(toPredict)
  testNumber = floor((units_number - predictNumber) * 0.20)
  trainNumber = units_number - testNumber - predictNumber

  print('number of...')
  print('total :', units_number)
  print('predict :', predictNumber)
  print('test :', testNumber)
  print('train :', trainNumber)
  print()
  
  if not predictNumber :
    print('fully known!')
    print('_____________________________________________')
    return

  toTest = np.zeros(Y.shape,dtype=bool)
  toTest[np.random.choice(np.where(toPredict == False)[0], testNumber, replace=False)] = True

  toTrain = ~(toTest | toPredict)
  
  is_fully_nan = lambda array : bool(np.prod(np.isnan(array)))
  fully_nan_features = np.array(list(map(is_fully_nan, X[toPredict].T)))

  predict_set = toPredict[:,np.newaxis] & (~fully_nan_features)[np.newaxis,:]
  train_set = toTrain[:,np.newaxis] & (~fully_nan_features)[np.newaxis,:]
  test_set = toTest[:,np.newaxis] & (~fully_nan_features)[np.newaxis,:]

  reg = method().fit(imputer.fit_transform(X[train_set].reshape(trainNumber, -1)), Y[toTrain])
  trainScore = reg.score(imputer.fit_transform(X[train_set].reshape(trainNumber, -1)), Y[toTrain])
  testScore = reg.score(imputer.fit_transform(X[test_set].reshape(testNumber, -1)), Y[toTest])
  print('accuracy on train data:', trainScore)
  print('accuracy on test data:', testScore)
  print('_____________________________________________')
  if trainScore < trust_edge or testScore < trust_edge :
    print('*'*20, 'Can not trust!')
    print('_____________________________________________')
    return
  
  properties[yLabel][toPredict] = reg.predict(imputer.fit_transform(X[predict_set].reshape(predictNumber, -1)))
  print('_____________________________________________')


# In[ ]:


def total_nan_rate() :
  return np.sum(np.isnan(properties.values)) / (units_number*len(train_select_features))


# ***تمام مراحل زیر با روش آزمون الخطا به بهترین نتیجه رسیده اند و متد ها و مرز های اطمینان با روش آزمون و خطا انتخاب شده اند.***

# عملیات توضیح داده شده در بالا چند بار با متد های رگرسیون متفاوت و مرز های اطمینان متفاوت (نزولی) روی دیتا اجرا می شوند :

# In[ ]:


print('total nan rate :', total_nan_rate())
print('_____________________________________________')

from sklearn.linear_model import BayesianRidge

for i in range(len(sorted_selected_train_features)) :
  guess_feature(i, method=BayesianRidge)
  print('total nan rate :', total_nan_rate())
  print('_____________________________________________')


# In[ ]:


for i in range(len(sorted_selected_train_features)) :
  guess_feature(i)
  print('total nan rate :', total_nan_rate())
  print('_____________________________________________')


# In[ ]:


for i in range(len(sorted_selected_train_features)) :
  guess_feature(i, trust_edge=0.5)
  print('total nan rate :', total_nan_rate())
  print('_____________________________________________')


# In[ ]:


for i in range(len(sorted_selected_train_features)) :
  guess_feature(i, trust_edge=0.5, method=BayesianRidge)
  print('total nan rate :', total_nan_rate())
  print('_____________________________________________')


# In[ ]:


for i in range(len(sorted_selected_train_features)) :
  guess_feature(i, trust_edge=0.3)
  print('total nan rate :', total_nan_rate())
  print('_____________________________________________')


# با توجه به گزارش بالا به درصد نقصان 0.45 رسیدیم که مقداری قابل قبول است. اما برای اجرای الگوریتم های آموزشی به دیتای کامل نیاز داریم و این موضوع را در قسمت بعد حل می کنیم.

# #### fast Imputing

# در آخرین قسمت های بالا به مرز های پایینی از اطمینان رسیدیم و دیگر گذاشتن نام آموزش بر روی آن الگوریتم ها درست نبود. پس برای کامل کردن این درصد کم دیتا از روش جاگذاری با میانگین استفاده می کنیم و دیتای کامل را بدست می آوریم.

# In[ ]:


numpyCopleteProperties = SimpleImputer().fit_transform(properties.values)
print('total nan rate :', np.sum(np.isnan(numpyCopleteProperties) / (units_number*len(train_select_features))))


# In[ ]:


numpyCopleteProperties


# همانطور که مشاهده می شود دیتا کامل شده است.

# ### Save New DataSet

# با توجه به رم استفاده شده در این بخش. این نتایج را ذخیره کرده و در قسمت بعد با لود کردن آن عملیات پیشبینی خطا را انجام می دهیم.

# In[ ]:


import json

new_data_set = dict(zip(properties_2017.index, numpyCopleteProperties.tolist()))

with open("./new_data_set.txt","w") as f :
    f.write(json.dumps(new_data_set))


# In[ ]:


new_data_set


# In[ ]:


uploaded = drive.CreateFile({'title': 'new_data_set.txt'})
uploaded.SetContentFile('new_data_set.txt')
uploaded.Upload()
print('Uploaded file with ID {}'.format(uploaded.get('id')))

